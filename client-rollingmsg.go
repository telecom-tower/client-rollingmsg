// Copyright 2015 Jacques Supcik, HEIA-FR
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// 2015-07-29 | JS | First version
// 2015-11-18 | JS | Using WebSocket
// 2016-08-11 | JS | Version with Firebase DB instead of WebSockets

//
// Telecom Tower
//

package main

import (
	"flag"
	"github.com/BlueMasters/firebasedb"
	log "github.com/Sirupsen/logrus"
	"github.com/cenkalti/backoff"
	"github.com/vharitonsky/iniflags"
	"gitlab.com/geomyidae/ws2811client"
	"time"
)

const (
	columns           = 128
	rows              = 8
	defaultBrightness = 32
)

func main() {
	var firebaseURL = flag.String("firebase-url", "https://telecom-tower.firebaseio.com/currentBitmap", "Firebase URL")
	var brightness = flag.Int(
		"brightness", defaultBrightness,
		"Brightness between 0 and 255.")
	var debug = flag.Bool("debug", false, "be verbose")

	iniflags.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else {
		log.SetLevel(log.InfoLevel)
	}

	log.Infoln("Starting tower")

	backOff := backoff.NewExponentialBackOff()
	backOff.MaxElapsedTime = 0 // retry forever
	ref := firebasedb.NewReference(*firebaseURL).Retry(backOff)
	if ref.Error != nil {
		log.Fatal(ref.Error)
	}

	gw := ws2811client.LocalClient()
	gw.Brightness = *brightness
	gw.Open(1000 * time.Millisecond)

	// start the tower goroutines pipeline
	closer := make(chan chan error)
	done := painter(
		sequencer(
			feeder(ref, closer),
			rows,
			columns),
		gw,
		rows,
		columns)

	<-done

	log.Infoln("Main loop terminated!")

	gw.Close()

}
